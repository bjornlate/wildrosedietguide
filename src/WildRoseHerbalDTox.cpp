// Tabbed pane project template
#include "WildRoseHerbalDTox.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/TextField>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/ListView>
#include <bb/cascades/HelpActionItem>
#include <bb/cascades/Container>
#include <bb/cascades/Page>
#include <bb/cascades/Sheet>
#include <bb/cascades/Menu>
#include <bb/data/DataSource>
#include <bb/data/XmlDataAccess>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeRequest>
#include "SearchableGroupDataModel.h"
#include <bb/cascades/advertisement/Banner>

using namespace bb::cascades::advertisement;
using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

WildRoseHerbalDTox::WildRoseHerbalDTox(bb::cascades::Application *app)
: QObject(app)
{

	// Registers the banner for QML
	qmlRegisterType<bb::cascades::advertisement::Banner>("bb.cascades.advertisement", 1, 0, "Banner");

	// create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    QString path(QDir::homePath() + "/shoppinglist.xml");
    // create root object for the UI
    root = qml->createRootObject<AbstractPane>();

    // set created root object as a scene
    app->setScene(root);

    connect(root,SIGNAL(save()), this, SLOT(onAboutToQuit()));

    ListView * guideListView = root->findChild<ListView*>("guideListView");
    guideListView->resetDataModel();

    // create a data model with sorting keys for lastname and firstname
    GroupDataModel *model = new GroupDataModel(QStringList() << "rec" << "title");
    model->setGrouping(ItemGrouping::ByFullValue);
    // load the xml data
    XmlDataAccess xda;
    //QVariant list = xda.load("guide.xml");
    QVariant list = xda.load(QDir::currentPath() +
            "/app/native/assets/guide.xml", "/guide/item");

    // add the data to the model
    model->insertList(list.value<QVariantList>());


    SearchableGroupDataModel * sgdm = new SearchableGroupDataModel(model, this);

    guideListView->setDataModel(sgdm);

    TextField *filterText = root->findChild<TextField*>("filterText");
    if(filterText){
    	connect(filterText, SIGNAL(textChanging(const QString)), sgdm, SLOT(setSearchQuery(QString)));
    }

    Container * emptyGuideLabel = root->findChild<Container*>("emptyGuideListMsg");
    if(emptyGuideLabel){
    	connect(sgdm, SIGNAL(isEmpty(bool)), emptyGuideLabel, SLOT(setVisible(bool)));
    }
//    Menu * menu = Menu::create();
//
//    HelpActionItem * aboutAction = HelpActionItem::create().title("About");
//    menu->setHelpAction(aboutAction);
//    connect(aboutAction, SIGNAL(triggered()), this, SLOT(onHelpTriggered()));
//    Application::instance()->setMenu(menu);

//    ListView * shoppingListView = root->findChild<ListView*>("shoppingListView");
//    shoppingListView->dataModel();

    connect(Application::instance(), SIGNAL(aboutToQuit()), this, SLOT(onAboutToQuit()));

    DataSource* ds = root->findChild<DataSource*>("shoppingListDataSource");
    if(ds){
    	ds->setSource(QUrl(path));
    	ds->load();
    }

    Sheet * addItemSheet = root->findChild<Sheet*>("addItemSheet");
    if(addItemSheet){
    	connect(addItemSheet, SIGNAL(addItem(QString, QString, QString)), this, SLOT(onAddItem(QString, QString, QString)));
    }

}

void WildRoseHerbalDTox::onHelpTriggered(){



	InvokeManager invokeManager;
	InvokeRequest request;
	request.setUri("http://wildroseproducts.com/");
	request.setAction("bb.action.OPEN");
	request.setTarget("sys.browser");
	invokeManager.invoke(request);

}

void WildRoseHerbalDTox::onAboutToQuit(){
	//Save the item
	ListView * shoppingListView = root->findChild<ListView*>("shoppingList");
	if(shoppingListView){
		GroupDataModel * gdm = static_cast<GroupDataModel*>(shoppingListView->dataModel());

		if(gdm){

			QVariantList items;

			int numberOfHeaders = gdm->childCount(QVariantList());

			for(int i=0;i<numberOfHeaders;++i){
				int numberOfChildren = gdm->childCount(QVariantList() << i);
				for(int j=0;j<numberOfChildren;++j){
					items << gdm->data(QVariantList() << i << j);
				}
			}

			QVariantMap itemsMap;
			itemsMap[".root"] = QVariant("items");
			itemsMap["item"] = QVariant(items);
			QVariant myData = QVariant(itemsMap);
			QFile file("data/shoppinglist.xml");
			if(file.open(QIODevice::WriteOnly)){
				// Create an XmlDataAccess object and save the data to the file
				XmlDataAccess xda;
				xda.save(myData, &file);
				if(xda.hasError()){
					qDebug() << xda.error().errorMessage();
				}
				file.close();
			}
		}
	}

}

void WildRoseHerbalDTox::onAddItem(QString title, QString section, QString description){
	ListView * shoppingListView = root->findChild<ListView*>("shoppingList");
	if(shoppingListView){
		GroupDataModel * gdm = static_cast<GroupDataModel*>(shoppingListView->dataModel());
		if(gdm){
			QVariantMap map;
			map["title"] = title;
			map["section"] = section;

			if(!description.isEmpty()){
				map["description"] = description;
			}
			gdm->insert(map);
		}
	}
}
