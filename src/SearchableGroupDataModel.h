/*
 * SearchableGroupDataModel.h
 *
 *  Created on: Jan 7, 2013
 *      Author: scottbirksted
 */

#ifndef SEARCHABLEGROUPDATAMODEL_H_
#define SEARCHABLEGROUPDATAMODEL_H_

#include <QObject>
#include <bb/cascades/DataModel>
#include <bb/cascades/GroupDataModel>


class SearchableGroupDataModel: public bb::cascades::DataModel {
	Q_OBJECT

signals:
	void isEmpty(bool);

public slots:
	void onDataLoaded(const QVariant & var);

	void setSearchQuery(QString query);
	void refresh();
	void removeAt(QVariantList indexPath);
	void removeList(QVariantList indexPaths);

	bool updateItem (const QVariantList &indexPath, const QVariantMap &item );

	void clear();

public:
	SearchableGroupDataModel(bb::cascades::GroupDataModel * sourceDataModel, QObject *parent = 0);
	virtual ~SearchableGroupDataModel();

	bb::cascades::GroupDataModel * sourceDataModel();


    virtual int childCount(const QVariantList& indexPath);
    virtual bool hasChildren(const QVariantList& indexPath);
    virtual QVariant data(const QVariantList& indexPath);
    virtual QString itemType(const QVariantList& indexPath);

    void onRemoved(QVariantList indexPath);

    void insert(const QVariantMap & map);
private:
    bb::cascades::GroupDataModel * m_sourceDataModel;
    bb::cascades::GroupDataModel * m_searchedDataModel;
    QString searchQuery;
};

#endif /* SEARCHABLEGROUPDATAMODEL_H_ */
