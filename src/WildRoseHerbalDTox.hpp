// Tabbed pane project template
#ifndef WildRoseHerbalDTox_HPP_
#define WildRoseHerbalDTox_HPP_

#include <QObject>

namespace bb {
	namespace cascades {
		class Application;
		class AbstractPane;
	}
}

/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class WildRoseHerbalDTox : public QObject
{
    Q_OBJECT
public:
    WildRoseHerbalDTox(bb::cascades::Application *app);
    virtual ~WildRoseHerbalDTox() {}

private slots:
	void onHelpTriggered();
	void onAboutToQuit();
	void onAddItem(QString title, QString section, QString description);
private:
	bb::cascades::AbstractPane *root;
};

#endif /* WildRoseHerbalDTox_HPP_ */
