/*
 * SearchableGroupDataModel.cpp
 *
 *  Created on: Jan 7, 2013
 *      Author: scottbirksted
 */

#include "SearchableGroupDataModel.h"

#include <QtCore>
#include <bb/cascades/ItemGrouping>
#include <bb/cascades/DataModelChangeType>

using namespace bb::cascades;

SearchableGroupDataModel::SearchableGroupDataModel(bb::cascades::GroupDataModel *sourceModel, QObject *parent)
: bb::cascades::DataModel(parent)
, m_sourceDataModel(sourceModel), m_searchedDataModel(new GroupDataModel())
{

	m_searchedDataModel->setSortingKeys(m_sourceDataModel->sortingKeys());
	m_searchedDataModel->setGrouping(m_sourceDataModel->grouping());

	connect(m_sourceDataModel, SIGNAL(sortingKeysChanged(QStringList)), m_searchedDataModel, SLOT(setSortingKeys(QStringList)));
	connect(m_sourceDataModel, SIGNAL(sortedAscendingChanged(bool)), m_searchedDataModel, SLOT(setSortedAscending(bool)));
	connect(m_sourceDataModel, SIGNAL(groupingChanged(bb::cascades::ItemGrouping::Type)), m_searchedDataModel, SLOT(setGrouping(bb::cascades::ItemGrouping::Type)));

	connect(m_sourceDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)));
	connect(m_searchedDataModel, SIGNAL(itemAdded(QVariantList)), this, SIGNAL(itemAdded(QVariantList)));

	connect(m_sourceDataModel, SIGNAL(itemRemoved(QVariantList)), this, SIGNAL(itemRemoved(QVariantList)));
	connect(m_searchedDataModel, SIGNAL(itemRemoved(QVariantList)), this, SIGNAL(itemRemoved(QVariantList)));

	connect(m_sourceDataModel, SIGNAL(itemUpdated(QVariantList)), this, SIGNAL(itemUpdated(QVariantList)));
	connect(m_searchedDataModel, SIGNAL(itemUpdated(QVariantList)), this, SIGNAL(itemUpdated(QVariantList)));

	connect(m_sourceDataModel, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
	            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), this, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
	            QSharedPointer<bb::cascades::DataModel::IndexMapper>)));

	connect(m_searchedDataModel, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
	            QSharedPointer<bb::cascades::DataModel::IndexMapper>)), this, SIGNAL(itemsChanged(bb::cascades::DataModelChangeType::Type,
	            QSharedPointer<bb::cascades::DataModel::IndexMapper>)));
}

SearchableGroupDataModel::~SearchableGroupDataModel() {
	// TODO Auto-generated destructor stub
}

GroupDataModel * SearchableGroupDataModel::sourceDataModel(){
	return m_sourceDataModel;
}

void SearchableGroupDataModel::onDataLoaded(const QVariant & var){
	if(var.canConvert<QVariantList>()){
//		QVariantList list;
//		foreach(QVariant var2, list){
			m_sourceDataModel->insertList(var.toList());
//		}
	}

}

int SearchableGroupDataModel::childCount(const QVariantList& indexPath){

	if(searchQuery.isEmpty()){
		return m_sourceDataModel->childCount(indexPath);
	}else{
		return m_searchedDataModel->childCount(indexPath);
	}
}
bool SearchableGroupDataModel::hasChildren(const QVariantList& indexPath){
	if(searchQuery.isEmpty()){
		return m_sourceDataModel->hasChildren(indexPath);
	}else{
		return m_searchedDataModel->hasChildren(indexPath);
	}
}
QVariant SearchableGroupDataModel::data(const QVariantList& indexPath){
	if(searchQuery.isEmpty()){
		return m_sourceDataModel->data(indexPath);
	}else{
		return m_searchedDataModel->data(indexPath);
	}

}
QString SearchableGroupDataModel::itemType(const QVariantList& indexPath){
	if(searchQuery.isEmpty()){
		return m_sourceDataModel->itemType(indexPath);
	}else{
		return m_searchedDataModel->itemType(indexPath);
	}
}


void SearchableGroupDataModel::refresh(){
	setSearchQuery(searchQuery);
}

void SearchableGroupDataModel::setSearchQuery(QString query){
	this->searchQuery = query;

	if(!searchQuery.isEmpty()){
		m_searchedDataModel->clear();

//		for (QVariantList indexPath = m_sourceDataModel->first(); !indexPath.isEmpty(); indexPath = m_sourceDataModel->after(indexPath) ){
//		    QVariant item = m_sourceDataModel->data(indexPath);
//		    if(item.canConvert<QVariantMap>()){
//		    	QVariantMap map = item.toMap();
//		    	foreach(QVariant value, map.values()){
//		    		if(value.toString().contains(searchQuery, Qt::CaseInsensitive)){
//		    			m_searchedDataModel->insert(map);
//		    		}
//		    	}
//		    }
//		}

		foreach(QVariantMap map, m_sourceDataModel->toListOfMaps()){
			foreach(QVariant value, map.values()){
				if(value.toString().contains(searchQuery, Qt::CaseInsensitive)){
					m_searchedDataModel->insert(map);
				}
			}
		}

		emit isEmpty(m_searchedDataModel->size() == 0);
	}else{
		emit isEmpty(m_sourceDataModel->size() == 0);
	}

	emit itemsChanged(bb::cascades::DataModelChangeType::Init);


}

void SearchableGroupDataModel::onRemoved(QVariantList indexPath){
	emit itemRemoved(indexPath);
}

void SearchableGroupDataModel::removeAt(QVariantList indexPath){
	if(!searchQuery.isEmpty()){
		QVariant var = m_searchedDataModel->data(indexPath);
		if(var.canConvert<QVariantMap>()){
			QVariantMap map = var.toMap();
			m_sourceDataModel->remove(map);
		}
		m_searchedDataModel->removeAt(indexPath);
		//emit itemRemoved(indexPath);
	}else{
		m_sourceDataModel->removeAt(indexPath);
		//emit itemRemoved(indexPath);
		//emit itemsChanged(bb::cascades::DataModelChangeType::Init);
	}
}

bool SearchableGroupDataModel::updateItem (const QVariantList &indexPath, const QVariantMap &item ){
	bool retVal = false;
	if(searchQuery.isEmpty()){
		retVal = m_sourceDataModel->updateItem(indexPath, item);
	}else{
		QVariant var = m_searchedDataModel->data(indexPath);
		if(var.isValid() && var.canConvert<QVariantMap>()){
			QVariantList srcIndexPath = m_sourceDataModel->findExact(var.toMap());
			if(srcIndexPath.length() > 0){
				retVal = m_sourceDataModel->updateItem(srcIndexPath, item);
			}
			refresh();
		}
	}
	emit itemsChanged(bb::cascades::DataModelChangeType::Init);
	return retVal;
}


void SearchableGroupDataModel::removeList(QVariantList indexPaths){
	qDebug() << "Size: " << indexPaths.size();
	for(int i=indexPaths.size()-1; i>= 0; --i){
		if(indexPaths.at(i).canConvert<QVariantList>()){
			removeAt(indexPaths.at(i).toList());
		}
	}
}

void SearchableGroupDataModel::insert(const QVariantMap & map){
	m_sourceDataModel->insert(map);
	//if(!searchQuery.isEmpty()){
	refresh();
	//}
}

void SearchableGroupDataModel::clear(){
	m_sourceDataModel->clear();
	refresh();
}

