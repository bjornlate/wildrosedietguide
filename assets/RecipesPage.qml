import bb.cascades 1.0

Page {
    id: recipePage
    Container {
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            leftPadding: 10
            rightPadding: 10
            ImageButton {
                defaultImageSource: "asset:///ic_previous.png"
                onClicked: {
                    webView.goBack()
                }
                verticalAlignment: VerticalAlignment.Center
                visible: webView.canGoBack
            }
            TextField {
                text: webView.url
                enabled: false
                verticalAlignment: VerticalAlignment.Center
            }
            topPadding: 10
            bottomPadding: 10
        }
        ProgressIndicator {
            value: webView.loadProgress
            visible: value < 100
            fromValue: 0
            toValue: 100
            horizontalAlignment: HorizontalAlignment.Center
            bottomPadding: 10
        }

        ScrollView {
            id: scrollView
            scrollViewProperties {
                scrollMode: ScrollMode.Vertical
                pinchToZoomEnabled: true
            }
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1.0
            }

            Container {
                background: Color.LightGray

                WebView {
                    id: webView
                    url: "http://pinterest.com/cavemandiet/recipes/"
                    
                    onMinContentScaleChanged: {
                        scrollView.scrollViewProperties.minContentScale = minContentScale;
                    }

                    onMaxContentScaleChanged: {
                        scrollView.scrollViewProperties.maxContentScale = maxContentScale;
                    }

                    onLoadProgressChanged: {
                    }
                    onLoadingChanged: {
                        if (loadRequest.status == WebLoadStatus.Started) {
                            //statusLabel.setText("Load started.")
                            //recipePage.removeAllActions()
                        } else if (loadRequest.status == WebLoadStatus.Succeeded) {
                            //statusLabel.setText("Load finished.")
                            //invoke.query.data = "Check out " + webView.title + " at " + webView.url
                            shareAction.query.uri = url
                            console.log(url)
                            shareAction.query.updateQuery()
                            //recipePage.addAction(shareAction, ActionBarPlacement.InOverflow);

                        } else if (loadRequest.status == WebLoadStatus.Failed) {
                            //statusLabel.setText("Load failed.")
                            //recipePage.removeAllActions()
                        }
                    }
                    attachedObjects: []
                }
            }
        }
    }
    actions: [
        InvokeActionItem {
            id: shareAction
            title: "Share this Recipe Page"
            query.uri: webView.url
        }
    ]
}