import bb.cascades 1.0

Sheet {
    id: helpSheet
    Page {
        titleBar: TitleBar {
            title: "Info"
            dismissAction: ActionItem {
                title: "Close"
                onTriggered: {
                    helpSheet.close()
                }
            }
        }
        ScrollView {
	        Container {
	            Container{
	                leftPadding: 20
	                rightPadding: 20
	                topPadding: 20
	                bottomPadding: 20
	                Label {
	                    multiline: true
	                    text: 
"<html><body>This app is an independent creation by Northern Nests, and is not affiliated with Wild Rose, or Trophic Canada Ltd.

This app is a companion to the Wild Rose Herbal D-Tox program.

<a href=\"http://wildroseproducts.com\"><span style=\"font-size:large;font-weight:600\">Wild Rose Herbal D-Tox Program</span></a>

<b>Characteristics and function:</b>
Internal cleansing is considered to be the cornerstone of good health by many natural physicians. 

Pollutants from the environment – found in the air, water and foods that we eat – as well as wastes produced from normal bodily processes, tend to accumulate within the body and lead to a state of congestion. To help maintain a healthy balance of assimilation and elimination, the Wild Rose Herbal D-Tox Program is uniquely designed to enhance all aspects of metabolism. Wild Rose Bliherb formula gently promotes bile production by the liver, supporting digestion and enhancing the elimination of toxins. Laxaherb, Cleansaherb and CL Herbal Extract additionally support the cleansing and elimination of wastes from the system.

The diet associated with the program is of great importance. During this period, people can eat all they want but the selection of food is quite important. The diet should consist of at least 80% alkaline and neutral ash foods with less than 20% acid ash foods. Referring to the following chart, one should eat less than 20% of the foods in Section 1 and 80% or more of foods from Sections 2 and 3 combined.

<b>If the food is not listed on this chart, you cannot eat it!</b> It is strongly recommended that no alcohol be consumed during this program. Although it is best not to drink coffee during this period of time, people can drink a maximum of two cups of black coffee per day. Herbal teas are acceptable. The addition of spice to the diet is acceptable, but the use of store-bought condiments, such as ketchup, should be avoided. Note: There are no breads or other flour products (e.g. breads, cakes, cookies, pastas or the like), dairy products (except butter), or tropical fruit allowed during this program. One should try to eliminate all foods that might congest the system during a detox.
</body></html>"
                }
            }
            
        }
    }
    }
}
