// Tabbed Pane project template
import bb.cascades 1.0
import bb.data 1.0
import bb.system 1.0

TabbedPane {
    id: tabbedPane
    showTabsOnActionBar: true

    property string shoppingListPath

    signal save

    property string section1: "Section 1: Protein / Acid Forming"
    property string section2: "Section 2: Starch / Alkaline Forming"
    property string section3: "Section 3: Natural / Bulk Forming"
    
    Menu.definition: MenuDefinition {
    	actions: [
    	    ActionItem{
    	    	title: "Info"
    	    	imageSource: "asset:///ic_info.png"
    	    	onTriggered: {
          		  	infoSheet.open();
                }
            }
    	]
    	
    }
    
    attachedObjects: [

        GroupDataModel {
            id: shoppingListDataModel
            sortingKeys: [ "rec", "title" ]
            grouping: ItemGrouping.ByFullValue
            onItemAdded: {
                emptyShoppingListMsg.visible = size() == 0
            }
            onItemRemoved: {
                emptyShoppingListMsg.visible = size() == 0
            }

        },
        DataSource {
            id: dataSource
            objectName: "shoppingListDataSource"
            //source: shoppingListPath
            query: "/items/item"

            //query: "select * from contact order by firstname, lastname"
            onDataLoaded: {
                shoppingListDataModel.insertList(data);
            }
        },
        InfoSheet {
            id: infoSheet
        },
        AddItemSheet {
            id: addItemSheet
            objectName: "addItemSheet"
        },
        SystemToast {
            id: pinterestToast
            body: "You can login to Pinterest and pin your favorite Recipes, look up ingredients in the Guide then add them to your Shopping List.\n\nYou can also share recipes you like from the side menu."
            position: SystemUiPosition.TopCenter
            
        }
    ]

    Tab {
        title: qsTr("Guide")
        imageSource: "asset:///ic_apple.png"

        Page {
            id: tab1

			actions: [
                InvokeActionItem {
                    id: reviewAction
                    title: "Add a Review"
                    query.invokeTargetId: "sys.appworld"
                    query.invokeActionId: "bb.action.OPEN"
                    query.uri: "appworld://content/27783423"
                }
            ]

            Container {
            	
                AdContainer{
                    
                }

                Container {
                    topPadding: 10
                    leftPadding: 20
                    rightPadding: 20
                    bottomPadding: 10
                    visible: true
                    TextField {
                        id: filterText
                        objectName: "filterText"
                        hintText: "Find"
                    }
                }

                Container {
                    id: emptyGuideListMsg
                    objectName: "emptyGuideListMsg"
                    topPadding: 20
                    bottomPadding: 20
                    leftPadding: 80
                    rightPadding: 80
                    visible: false

                    Label {
                        text: qsTr("Don't Eat it!")
                        textStyle.textAlign: TextAlign.Center
                        textStyle.fontSize: FontSize.Large
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                    Label {
                        text: qsTr("What you were looking for is not in the Guide.")
                        multiline: true
                        textStyle.textAlign: TextAlign.Center
                        textStyle.fontSize: FontSize.Medium
                        verticalAlignment: VerticalAlignment.Center

                    }
                    verticalAlignment: VerticalAlignment.Center
                }

                ListView {
                    id: guideListView
                    objectName: "guideListView"

                    function addToShoppingList(data) {
                        var indexPath = shoppingListDataModel.findExact(data); //([data.rec, data.name]);
                        if (indexPath.length == 0) {
                            shoppingListDataModel.insert(data)
                        }
                    }

                    dataModel: XmlDataModel {
                        source: "model.xml"
                    }

                    onTriggered: {
                        addToShoppingList(dataModel.data(indexPath));
                    }

                    listItemComponents: [
                        ListItemComponent {
                            type: "header"
                            Header {
                                title: ListItemData == "1" ? "Most Recommended" : ListItemData == "2" ? "Acceptable" : ListItemData == "3" ? "Not Recommended" : "Never Recommended"
                            }
                        },
                        ListItemComponent {
                            type: "item"
                            Container {

                                visible: ! ListItemData.hide
                                id: guideItem

                                ListItem.onSelectionChanged: {
                                    if (ListItem.selected) {
                                        guideItem.background = Color.create("#c9e0de");
                                    } else if (! ListItem.active) {
                                        guideItem.resetBackground();
                                    }
                                }

                                ListItem.onActivationChanged: {
                                    if (ListItem.active) {
                                        guideItem.background = Color.create("#c9e0de");
                                    } else if (! ListItem.selected) {
                                        guideItem.resetBackground();
                                    }
                                }

                                contextActions: [
                                    ActionSet {
                                        title: ListItemData.title

                                        ActionItem {
                                            title: "Add to Shopping List"
                                            imageSource: "asset:///cart2.png"
                                            onTriggered: {
                                                addAnim.play()
                                                guideItem.ListItem.view.addToShoppingList(guideItem.ListItem.data)
                                            }
                                        }
                                    }
                                ]

                                animations: [
                                    SequentialAnimation {
                                        id: addAnim
                                        TranslateTransition {
                                            toX: 1280
                                            duration: 300
                                            easingCurve: StockCurve.SineIn
                                        }
                                        FadeTransition {
                                            toOpacity: 0.0
                                            duration: 1
                                        }
                                        TranslateTransition {
                                            toX: 0
                                            duration: 0
                                        }
                                        FadeTransition {
                                            toOpacity: 1.0
                                            duration: 500
                                        }
                                    }

                                ]
                                Divider {
                                	opacity: 0.0
                                }
                                GuideItem {
                                }
                                Divider {
                                }
                            }
                        }
                    ]
                    layout: StackListLayout {
                        headerMode: ListHeaderMode.Sticky
                    }
                    focusPolicy: FocusPolicy.Default
                }
            }
        }
    }
    Tab {
        title: qsTr("Shopping")
        imageSource: "asset:///cart2.png"
        Page {
            id: tab2

            actions: [
                ActionItem {
                    title: "Save Shopping List"
                    imageSource: "asset:///ic_save.png"
                    onTriggered: {
                        tabbedPane.save();
                    }
                },
                ActionItem {
                    title: "Add Shopping Item"
                    imageSource: "asset:///ic_add.png"
                    onTriggered: {
                        addItemSheet.open()
                    }
                },
                DeleteActionItem {
                    title: "Clear Shopping List"
                    onTriggered: {
                        clearToast.show()
                    }

                    attachedObjects: [
                        SystemToast {
                            property string itemName
                            id: clearToast
                            body: "Clearing Shopping List"
                            button.label: "Undo"
                            position: SystemUiPosition.BottomCenter
                            onFinished: {
                                console.log("Clearing list");

                                if (! clearToast.buttonSelection()) {
                                    shoppingListDataModel.clear()
                                    console.log("Cleared list");
                                    emptyShoppingListMsg.visible = true
                                }
                            }
                        }
                    ]
                }
            ]

            Container {
            	AdContainer{}

                Container {

                    layout: DockLayout {

                    }
                    Container {
                        id: emptyShoppingListMsg
                        topPadding: 20
                        bottomPadding: 20
                        leftPadding: 80
                        rightPadding: 80
                        ImageView {
                            imageSource: "asset:///caveman_emptycart.png"
                            horizontalAlignment: HorizontalAlignment.Center
                        }
                        Label {
                            text: qsTr("Your Shopping List is empty")
                            multiline: true
                            textStyle.textAlign: TextAlign.Center
                            textStyle.fontSize: FontSize.Large
                            horizontalAlignment: HorizontalAlignment.Center
                        }
                        Label {
                            text: qsTr("You can add items from the Guide list by tapping on them")
                            multiline: true
                            textStyle.textAlign: TextAlign.Center
                            textStyle.fontSize: FontSize.Small
                            horizontalAlignment: HorizontalAlignment.Center
                        }
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                    }

                    ListView {
                        id: shoppingList
                        objectName: "shoppingList"
                        dataModel: shoppingListDataModel

                        layout: StackListLayout {
                            headerMode: ListHeaderMode.Sticky
                        }
                        
                        onCreationCompleted: {
                        }

                        function removeFromShoppingList(indexPath) {
                            shoppingListDataModel.removeAt(indexPath)
                        }
                        function updateInShoppingList(indexPath, data) {
                            shoppingListDataModel.updateItem(indexPath, data)
                        }

                        listItemComponents: [
                            ListItemComponent {
                                type: "header"
                                Header {
                                    title: ListItemData == "1" ? "Most Recommended" : ListItemData == "2" ? "Acceptable" : ListItemData == "3" ? "Not Recommended" : ListItemData == "4" ? "Never Recommended" : "General Shopping Items"
                                }
                            },
                            ListItemComponent {
                                type: "item"
                                Container {
                                    id: shoppingListItem

                                    ListItem.onSelectionChanged: {
                                        if (ListItem.selected) {
                                            shoppingListItem.background = Color.create("#c9e0de");
                                        } else if (! ListItem.active) {
                                            shoppingListItem.resetBackground();
                                        }
                                    }

                                    ListItem.onActivationChanged: {
                                        if (ListItem.active) {
                                            shoppingListItem.background = Color.create("#c9e0de");
                                        } else if (! ListItem.selected) {
                                            shoppingListItem.resetBackground();
                                        }
                                    }

                                    contextActions: [
                                        ActionSet {
                                            title: ListItemData.title
                                            DeleteActionItem {
                                                title: "Remove from Shopping List"
                                                onTriggered: {
                                                    shoppingListItem.ListItem.view.removeFromShoppingList(shoppingListItem.ListItem.indexPath)
                                                }
                                            }
                                        }
                                    ]
                                    Divider {
                                        opacity: 0.0
                                    }
                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }
                                        GuideItem {
                                        }
                                        rightPadding: 20
                                        CheckBox {
                                            verticalAlignment: VerticalAlignment.Top
                                            checked: ListItemData.check == "true"
                                            onCheckedChanged: {
                                                console.log(ListItemData.title + " checked changed")
                                                var data = ListItemData;
                                                data.check = checked ? "true" : "false"
                                                shoppingListItem.ListItem.view.updateInShoppingList(shoppingListItem.ListItem.indexPath, data)
                                            }
                                        }
                                    }
                                    Divider {
                                    }
                                }
                            }

                        ]
                    }
                }
            }
        }
    }

    Tab{
        id: recipeTab
        title: "Recipes"
        imageSource: "asset:///ic_apron.png"
        RecipesPage {
            
        }
        
    }
    
//    Tab {
//        title: qsTr("Info")
//        imageSource: "asset:///ic_info.png"
//        InfoPage{}
//    }

    onCreationCompleted: {
        // dataSource.load();

        // this slot is called when declarative scene is created
        // write post creation initialization here
        console.log("TabbedPane - onCreationCompleted()")

        // enable layout to adapt to the device rotation
        // don't forget to enable screen rotation in bar-bescriptor.xml (Application->Orientation->Auto-orient)
        OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.All;
    }
    property bool shownPinterestToast: false
    onActiveTabChanged: {
        if(activeTab == recipeTab && !shownPinterestToast){
            pinterestToast.show()
            shownPinterestToast = true
        }
    }
}
