import bb.cascades 1.0

Sheet{
    id: infoSheet
    Page {

    titleBar: TitleBar {
        title: "Help and Info"
        dismissAction: ActionItem {
            title: "Close"
            onTriggered: {
                infoSheet.close()
            }
        }
    }


        ScrollView {
            Container {
                Header {
                    title: qsTr("Overview")
                }
                Container {
                    leftPadding: 20
                    rightPadding: 20
                    visible: true
                    Label {
                        multiline: true
                        text: "<html><body>This <a href=\"appworld://content/27783423\">Caveman Diet Guide</a> app was created by <a href='appworld://vendorpage/70931'>Scott Birksted</a>.
Please leave me a <a href=\"appworld://content/27783423\">review</a>!
                    
The <a href='http://en.wikipedia.org/wiki/Paleolithic_diet'>Caveman Diet, aka Paleolithic or Paleo diet</a> is a healthy whole foods diet similar to the alkaline diet.

Use the <b>Recipe</b> tab to inspire your taste buds, then look up the ingredients in the <b>Guide</b> add them to your <b>Shopping List</b> by holding down on an item in the guide and choosing the shopping basket icon.
</body></html>"

                    }
                }
                Header {
                    title: "Using the Guide"
                }
                Container{
                    leftPadding: 20
                    rightPadding: 20
                    
                    Label {
                        visible: true
                        text: "<html><body>The Header title in the Guide list indicates a foods dietary recommendation, for example <b>Fish</b> is one of the <b>Most Recommended</b> foods for the program.</body></html>"
                        multiline: true;
                    }
                    Container{
                        background: Color.DarkGray//Color.create("#FFFFFF")
                        topPadding: 4
                        bottomPadding: 4
                        leftPadding: 4
                        rightPadding: 4
                        horizontalAlignment: HorizontalAlignment.Center
                    	ImageView {
	                        horizontalAlignment: HorizontalAlignment.Center
	                        preferredWidth: 690
	                        scalingMethod: ScalingMethod.AspectFit
	                        imageSource: "asset:///listitem.png"
	                        visible: true
	                    }
                    }
                    
                    Label{
                        text: "<html><body>The percentage indicates how much of this food you can eat on the diet, for example:
- <b>Garlic</b> is a food that can help make up <b>80%</b> of your diet.
- <b>Fish</b> should make up <b>less than 20%</b> of your diet. (Except <b>Fish</b> is a special exception, shown in the note.)

The color of the percentage indicates the Recommendation. <span style='color:#7ccd24'>Green</span> is great, <span style='color:red' >Red</span> is bad.

The last line is the Section of the diet the food is part of: <b>Protein, Starch or Neutral</b>
                        </body></html>"
                        multiline: true
                    }
                    
                }
                Header {
                    title: qsTr("Herbal D-Tox Program")
                    visible: false
                }
                Container {
                    leftPadding: 20
                    rightPadding: 20
                    visible: false
                    Label {
                        multiline: true
                        text: "<html><body>For more information about the Wild Rose Herbal D-Tox, or to find a location where you can order the kit, please visit the wild rose website:

<a href=\"http://wildroseproducts.com\"><span style=\"font-size:medium;font-weight:600\">http://wildroseproducts.com</span></a>

<b>Characteristics and function:</b>
Internal cleansing is considered to be the cornerstone of good health by many natural physicians. 

Pollutants from the environment – found in the air, water and foods that we eat – as well as wastes produced from normal bodily processes, tend to accumulate within the body and lead to a state of congestion. To help maintain a healthy balance of assimilation and elimination, the Wild Rose Herbal D-Tox Program is uniquely designed to enhance all aspects of metabolism. Wild Rose Bliherb formula gently promotes bile production by the liver, supporting digestion and enhancing the elimination of toxins. Laxaherb, Cleansaherb and CL Herbal Extract additionally support the cleansing and elimination of wastes from the system.

The diet associated with the program is of great importance. During this period, people can eat all they want but the selection of food is quite important. The diet should consist of at least 80% alkaline and neutral ash foods with less than 20% acid ash foods. Referring to the following chart, one should eat less than 20% of the foods in Section 1 and 80% or more of foods from Sections 2 and 3 combined.

<b>If the food is not found in the guide, you cannot eat it!</b> It is strongly recommended that no alcohol be consumed during this program. Although it is best not to drink coffee during this period of time, people can drink a maximum of two cups of black coffee per day. Herbal teas are acceptable. The addition of spice to the diet is acceptable, but the use of store-bought condiments, such as ketchup, should be avoided. Note: There are no breads or other flour products (e.g. breads, cakes, cookies, pastas or the like), dairy products (except butter), or tropical fruit allowed during this program. One should try to eliminate all foods that might congest the system during a detox.
</body></html>"
                    }
                }
            }
        }
    }
}