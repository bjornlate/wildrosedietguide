import bb.cascades 1.0
import bb.cascades.advertisement 1.0

Container {
    horizontalAlignment: HorizontalAlignment.Fill
    background: Color.Black
    id: adContainer
    Banner {
        zoneId: 117145
        refreshRate: 60
        preferredWidth: 720
        preferredHeight: 90
        transitionsEnabled: true
        //placeHolderURL: "asset:///placeholder_728x90.png"
        backgroundColor: adContainer.background
        borderColor: Color.Transparent
        borderWidth: 0
        horizontalAlignment: HorizontalAlignment.Center
    }
}
