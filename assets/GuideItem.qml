import bb.cascades 1.0

Container {
    leftPadding: 20
    rightPadding: 20

    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        Label {
            text: ListItemData.title
            bottomMargin: 8
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 9
        }
        Label {
            property string section: ListItemData.section
            text: section == "1" ? "< 20%" : "80%"
            textStyle.color: ListItemData.rec == "1" ? Color.create("#7ccd24") : Color.Black
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 8.5
            visible: ListItemData.rec == "2" | ListItemData.rec == "1"
            verticalAlignment: VerticalAlignment.Bottom
        }
        Label {
            visible: ListItemData.rec == "3" | ListItemData.rec == "4"
            text: ListItemData.rec == "3" ? "Never" : "Avoid"
            verticalAlignment: VerticalAlignment.Bottom
            textStyle.fontSize: FontSize.PointValue
            textStyle.fontSizeValue: 8.5
            textStyle.color: Color.create("#FF3333")
        }
    }
    Label {
        topMargin: 8
        rightMargin: 20
        visible: text.length > 0
        text: ListItemData.description
        multiline: true
        textStyle.fontStyle: FontStyle.Italic
        textStyle.fontSize: FontSize.PointValue
        textStyle.fontSizeValue: 6.5
        //textStyle.color: Color.DarkGray
    }
    Label {
        topMargin: 0
        rightMargin: 20
        visible: text.length > 0
        
        function sectionText(){
            var str = "Section " + ListItemData.section + ": ";
            if(ListItemData.section == "1"){
                str+= "Protein / Acid Forming";
            }else if(ListItemData.section == "2"){
                str+= "Starch / Alkaline Forming";
            }else if(ListItemData.section == "3"){
                str+= "Neutral / Bulk Forming";
            }else{
                str = "";
            }
            return str;
        }
        
        text: sectionText()
        textStyle.fontStyle: FontStyle.Default
        textStyle.fontSize: FontSize.PointValue
        textStyle.fontSizeValue: 6.5
        //textStyle.color: Color.DarkGray
    }
}
                                