// Tabbed Pane project template
import bb.cascades 1.0
import bb.data 1.0

TabbedPane {
    id: tabbedPane
    showTabsOnActionBar: true
    
    property string shoppingListPath;
    
    signal save;
    
    // Add the application menu using a MenuDefinition
    Menu.definition: MenuDefinition {
        // Add a Help action
        helpAction: HelpActionItem {
            onTriggered: {
                aboutSheet.open()
            }    
        }
        
    } // end of MenuDefinition
    
    
    property string section1: "Section 1: Protein / Acid Forming"
    property string section2: "Section 2: Starch / Alkaline Forming"
    property string section3: "Section 3: Natural / Bulk Forming"
    attachedObjects: [

        GroupDataModel{
            id: shoppingListDataModel
            sortingKeys: ["rec"]
            onItemAdded: {
                emptyShoppingListMsg.visible = size() == 0
            }
            onItemRemoved: {
                emptyShoppingListMsg.visible = size() == 0 
            }
        },
        DataSource {
          id: dataSource
          objectName: "shoppingListDataSource"
          //source: shoppingListPath
          query: "/items/item"
          
          //query: "select * from contact order by firstname, lastname"
          onDataLoaded: {
              shoppingListDataModel.insertList(data);
          }
        },
        HelpSheet {
            id: aboutSheet
        }
    ]
    
    
    Tab {
        title: qsTr("Guide")
        imageSource: "/usr/share/icons/bb_action_file.png"
        
        Page {
            id: tab1

            Container {
                Container{
                    topPadding: 20
                    leftPadding: 20
                    rightPadding: 20
                    bottomPadding: 20
	                visible: true
	                TextField{
	                    id: filterText
	                    objectName: "filterText"
	                    hintText: "Find"
	                }
                }
                ListView {
                    id: guideListView
                    objectName: "guideListView"
                    function addToShoppingList(data){
                        shoppingListDataModel.insert(data)
                        //shoppingListDataModel.insert(data);
                    }
                    
                    dataModel: XmlDataModel {
                        source: "model.xml"
                    }
                    listItemComponents: [
                        ListItemComponent {
                            type: "header"
                            Header{
                                title: ListItemData == "1" ? "Most Recommended" : ListItemData == "2" ? "Acceptable During Program" : ListItemData == "3" ? "Not Recommended During Program" : "Never Recommended" 
                            }
                        },
                        ListItemComponent {
                            type: "item"
                            Container {
                                
                                visible: !ListItemData.hide
	                            id: guideItem
                                contextActions : [
                                    ActionSet{
                                        ActionItem {
                                            title: "Add to Shopping List"
                                            imageSource: "/usr/share/icons/bb_action_forward.png"
                                            onTriggered: {
                                                guideItem.ListItem.view.addToShoppingList(guideItem.ListItem.data)
                                            }
                                        }
                                    }
                                ]
                                Divider{}
                                Container {
                                    leftPadding: 20
                                    rightPadding: 20

                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }
                                        Label {
                                            text: ListItemData.title
                                            bottomMargin: 8
                                            layoutProperties: StackLayoutProperties {
                                                spaceQuota: 1
                                            }
                                            textStyle.fontSize: FontSize.PointValue
                                            textStyle.fontSizeValue: 10
                                        }
                                        Label {
                                            property string section: ListItemData.section 
                                            text: section == "1" ? "< 20%" : "80%"
                                            textStyle.color: ListItemData.rec == "most" ? Color.DarkGreen : Color.Black
                                            textStyle.fontSize: FontSize.PointValue
                                            textStyle.fontSizeValue: 8.5
                                            visible: ListItemData.rec == "2" | ListItemData.rec == "1"
                                            verticalAlignment: VerticalAlignment.Bottom
                                        }
                                        Label{                                                                                        
                                            visible: ListItemData.rec == "3" | ListItemData.rec == "4"
                                            text: ListItemData.rec == "3" ? "Never" : "Avoid"
                                            verticalAlignment: VerticalAlignment.Bottom
                                            textStyle.fontSize: FontSize.PointValue
                                            textStyle.fontSizeValue: 8.5
                                            textStyle.color: Color.Red
                                        }
                                    }
                                    Label {
                                        topMargin: 8
                                        rightMargin: 20
                                        visible: text.length > 0
                                        text: ListItemData.description
                                        multiline: true
                                        textStyle.fontStyle: FontStyle.Italic
                                        textStyle.fontSize: FontSize.PointValue
                                        textStyle.fontSizeValue: 6.5
                                        textStyle.color: Color.DarkGray
                                    }
                                    Label {
                                        topMargin: 0
                                        rightMargin: 20
                                        //visible: text.length > 0
                                        text: "Section " + ListItemData.section + ": " + (ListItemData.section == "1" ? "Protein / Acid Forming" : (ListItemData.section == "2" ? "Starch / Alkaline Forming" : "Neutral / Bulk Forming")) 
                                        textStyle.fontStyle: FontStyle.Default
                                        textStyle.fontSize: FontSize.PointValue
                                        textStyle.fontSizeValue: 6.5
                                        textStyle.color: Color.DarkGray
                                    }
                                }
                                Divider {
                                }
                            }
                        }
                    ]
                    layout: StackListLayout {
                        headerMode: ListHeaderMode.Sticky
                    }
                    focusPolicy: FocusPolicy.Default
                }
            }
        }
    }
    Tab {
        title: qsTr("Shopping List")
        imageSource: "/usr/share/icons/bb_action_markassaved.png"
        Page {
            id: tab2

            actions: [
                ActionItem {
                    title: "Save"
                    onTriggered: {
                        tabbedPane.save();
                    }
                },ActionItem{
                    title: "Add Shopping Item"
                    onTriggered: {
                        
                    }
                },
                DeleteActionItem {
                    title: "Clear Shopping List"
                    onTriggered: {
                        shoppingListDataModel.clear()
                    }
                }
            ]
            
            Container {
                layout: DockLayout {
                
                }
                Container {
                    id: emptyShoppingListMsg
                    topPadding: 20
                    bottomPadding: 20
                    leftPadding: 80
                    rightPadding: 80
                    Label {
                        text: qsTr("You have no items in your Shopping List. \n\nYou can add an item from the Guide by long pressing it and choosing \"Add to Shopping List\" from the Menu.")
                        multiline: true
                        textStyle.textAlign: TextAlign.Left
                    }
                    verticalAlignment: VerticalAlignment.Center   
                }
                
               ListView{
                   id: shoppingList
                   objectName: "shoppingList"
                   dataModel: shoppingListDataModel
                   
                   onCreationCompleted: {
                   }
                   
                   function removeFromShoppingList(indexPath){
                       shoppingListDataModel.removeAt(indexPath)
                   }
                   
                   listItemComponents: [
                       ListItemComponent {
                           type: "header"
                           Header{
                               title: ListItemData == "1" ? "Most Recommended" : ListItemData == "2" ? "Acceptable During Program" : ListItemData == "3" ? "Not Recommended During Program" : "Never Recommended" 
                           }
                       },
                       ListItemComponent {
                           type: "item"
                           Container{
                               id: shoppingListItem
                               contextActions : [
                                   ActionSet{
                                       DeleteActionItem {
                                           title: "Remove from Shopping List"
                                           onTriggered: {
                                               shoppingListItem.ListItem.view.removeFromShoppingList(shoppingListItem.ListItem.indexPath)
                                           }
                                       }
                                   }
                               ]
                               
                               Divider{}
                               Container{
	                               layout: StackLayout {
	                                   orientation: LayoutOrientation.LeftToRight
	                               }
	                               leftPadding: 20
                                   CheckBox {
                                       verticalAlignment: VerticalAlignment.Center
                                       checked: ListItemData.check
                                       onCheckedChanged: {
                                           ListItemData.check = checked ? "true" : "false"
                                       }
                                   }
                                   Container {
                                       leftPadding: 10
                                       rightPadding: 20
                                       
                                       Container {
                                           layout: StackLayout {
                                               orientation: LayoutOrientation.LeftToRight
                                           }
                                           Label {
                                               text: ListItemData.title
                                               bottomMargin: 8
                                               layoutProperties: StackLayoutProperties {
                                                   spaceQuota: 1
                                               }
                                               textStyle.fontSize: FontSize.PointValue
                                               textStyle.fontSizeValue: 10
                                           }
                                           Label {
                                               property string section: ListItemData.section 
                                               text: section == "1" ? "< 20%" : "80%"
                                               textStyle.color: ListItemData.rec == "most" ? Color.DarkGreen : Color.Black
                                               textStyle.fontSize: FontSize.PointValue
                                               textStyle.fontSizeValue: 8.5
                                               visible: ListItemData.rec == "acpt" | ListItemData.rec == "most"
                                               verticalAlignment: VerticalAlignment.Bottom
                                           }
                                           Label{                                                                                        
                                               visible: ListItemData.rec == "nvr" | ListItemData.rec == "not"
                                               text: ListItemData.rec == "nvr" ? "Never" : "Avoid"
                                               verticalAlignment: VerticalAlignment.Bottom
                                               textStyle.fontSize: FontSize.PointValue
                                               textStyle.fontSizeValue: 8.5
                                               textStyle.color: Color.Red
                                           }
                                       }
                                       Label {
                                           topMargin: 8
                                           rightMargin: 20
                                           visible: text.length > 0
                                           text: ListItemData.description
                                           multiline: true
                                           textStyle.fontStyle: FontStyle.Italic
                                           textStyle.fontSize: FontSize.PointValue
                                           textStyle.fontSizeValue: 6.5
                                           textStyle.color: Color.DarkGray
                                       }
                                       Label {
                                           topMargin: 0
                                           rightMargin: 20
                                           //visible: text.length > 0
                                           text: "Section " + ListItemData.section + ": " + (ListItemData.section == "1" ? "Protein / Acid Forming" : (ListItemData.section == "2" ? "Starch / Alkaline Forming" : "Neutral / Bulk Forming")) 

                                           textStyle.fontStyle: FontStyle.Default
                                           textStyle.fontSize: FontSize.PointValue
                                           textStyle.fontSizeValue: 6.5
                                           textStyle.color: Color.DarkGray
                                       }
                                   }
                               }
                               Divider{}
                           }
                       }
                   
                   ]
                }
            }
        }
    }
    
    onCreationCompleted: {
        // dataSource.load();
        
        // this slot is called when declarative scene is created
        // write post creation initialization here
        console.log("TabbedPane - onCreationCompleted()")

        // enable layout to adapt to the device rotation
        // don't forget to enable screen rotation in bar-bescriptor.xml (Application->Orientation->Auto-orient)
        OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.All;
    }
}
