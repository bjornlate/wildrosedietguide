import bb.cascades 1.0

Sheet {
    id: addSheet
    signal addItem(string title, string section, string description)
    Page {
        titleBar: TitleBar {
            title: "New Item"
            acceptAction: ActionItem {
                title: "Add"
                onTriggered:{
                    if(title.text.length > 0){
                        addItem(title.text, section.selectedValue, description.text)
	                    title.resetText()
	                    description.resetText()
	                    section.setSelectedIndex(0)
	                    addSheet.close()
                    }
                }
            }
            dismissAction: ActionItem {
                title: "Cancel"
                onTriggered: {
                    title.resetText()
                    description.resetText()
                    section.setSelectedIndex(0)
                    
                    addSheet.close()
                }
            }
        }
        Container {
            topPadding: 20
            rightPadding: 20
            bottomPadding: 20
            leftPadding: 20
            Label {
                text: "You can add other items outside the program to your shopping list."
                multiline: true
            }
            TextField {
                id: title
                objectName: "title"
                hintText: "Name"
            }
            DropDown {
                id: section
                objectName: "section"
                title: "Section"
                options: [
                    
                    Option{
                        value: "5"
                        text: "General Shopping Items"
                        selected: true
                    },
                    Option{
                        value: "1"
                        text: "Most Recommended"
                    },
                    Option{
                        value: "2"
                        text: "Acceptable During Program"
                    },
                    Option{
                        value: "3"
                        text: "Not Recommended During Program"
                    },
                    Option{
                        value: "4"
                        text: "Never Recommended"
                    }
                ]
            }
            TextArea{
                id: description
                hintText: "Optional Description"
            }
        }
    }
}
